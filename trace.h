#ifndef FILEDEF_TRACE
#define FILEDEF_TRACE
#ifdef DEBUGTRACE
#ifndef IOSTREAM_INC
#define IOSTREAM_INC
#include <iostream>
#include <sstream>
#endif //IOSTREAM_INC
#define TRACE(SIDE,X) std::cout<<"TRACE["<<SIDE<<"]\x9::"<<X<<std::endl;

#ifndef TRACE_MARKER
#define TRACE_MARKER
template <size_t iLineIndex>
class __OrderCodeMark_t
{
	__OrderCodeMark_t();
	__OrderCodeMark_t(const __OrderCodeMark_t &);
	__OrderCodeMark_t &operator =(const __OrderCodeMark_t &);
	const std::string _sFilename;
	const std::string _sFunction;
public:
	__OrderCodeMark_t(const std::string &crsFilename,const std::string &crsFunction=std::string())
		:_sFilename(crsFilename),_sFunction(crsFunction)
	{TRACE("CodeMarker::"<<_sFilename<<" @ "<<iLineIndex<<" @ "<<_sFunction,"born");}
	~__OrderCodeMark_t()					  			      
	{TRACE("CodeMarker::"<<_sFilename<<" @ "<<iLineIndex<<" @ "<<_sFunction,"dead");}
};
#define TOKENPASTE_(x, y) x ## y
#define TOKENPASTE(x, y) TOKENPASTE_(x, y)
#ifdef USEPRETTY_FUNCTION
#define _FOO __PRETTY_FUNCTION__
#else
#define _FOO __FUNCTION__
#endif //__PRETTY_FUNCTION__
#define CODEMARK const __OrderCodeMark_t<__LINE__> TOKENPASTE(cxCodeLineMarker_,__LINE__)(__FILE__,_FOO);
#define CODEMARK const __OrderCodeMark_t<__LINE__> TOKENPASTE(cxCodeLineMarker_,__LINE__)(__FILE__,_FOO);
#endif //TRACE_MARKER

#else  //DEBUGTRACE
#define TRACE(SIDE,X)
#define CODEMARK
#endif //DEBUGTRACE
#endif //FILEDEF_TRACE